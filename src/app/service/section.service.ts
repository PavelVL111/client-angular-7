import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Section} from '../model/section-model';

@Injectable({
  providedIn: 'root'
})
export class SectionService {
  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getSectionsByCourse(id: number): Observable<Section[]> {
    return this.http.get<Section[]>(this.userUrl + 'allsection/' + id);
  }

  public getSection(id: number): Observable<Section> {
    return this.http.get<Section>(this.userUrl + 'section/' + id);
  }

  public createSection(idCourse: number): Observable<Section> {
    return this.http.get<Section>(this.userUrl + 'create/createsection/' + idCourse);
  }

  public deleteSection(idSection: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'create/deletesection/' + idSection);
  }

  public updateSection(section: Section): Observable<Section> {
    return this.http.post<Section>(this.userUrl + 'create/updatesection', section);
  }

}
