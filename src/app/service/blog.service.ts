import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getBlogs(): Observable<Blog[]> {
    return this.http.get<Blog[]>(this.userUrl + 'blog/allpost');
  }

  public getPost(id: number): Observable<Blog> {
    return this.http.get<Blog>(this.userUrl + 'blog/post/' + id);
  }

}
