import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';
import {Test} from '../model/test.model';
import {Answer} from '../model/answer.model';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getAnswers(id: number): Observable<Answer[]> {
    return this.http.get<Answer[]>(this.userUrl + 'answer/allanswer/' + id);
  }

  //
  public getAnswersForTest(id: number): Observable<Answer[]> {
    return this.http.get<Answer[]>(this.userUrl + 'answer/allanswer/fortest/' + id);
  }

  public createAnswer(id: number): Observable<Answer> {
    return this.http.get<Answer>(this.userUrl + 'create/createanswer/' + id);
  }

  public deleteAnswer(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'create/deleteanswer/' + id);
  }

  public updateAnswer(question: Answer): Observable<boolean> {
    return this.http.post<boolean>(this.userUrl + 'create/updateanswer', question);
  }
}
