import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../model/user.model';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AuthorizationLogerService {

  constructor(private http: HttpClient) {}
  private userUrl = 'http://localhost:8080/';
  public getAuthorizationStatus(): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'islogin');
  }

}
