import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';
import {Test} from '../model/test.model';
import {Question} from '../model/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  //
  public getQuestions(id: number): Observable<Question[]> {
    return this.http.get<Question[]>(this.userUrl + 'question/allquestion/' + id);
  }

  public createQuestion(id: number): Observable<Question> {
    return this.http.get<Question>(this.userUrl + 'create/createquestion/' + id);
  }

  public deleteQuestion(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'create/deletequestion/' + id);
  }

  public updateQuestion(question: Question): Observable<boolean> {
    return this.http.post<boolean>(this.userUrl + 'create/updatequestion', question);
  }
}
