import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Course} from '../model/course-model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SubscribeCoursesService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public isSubscribeCourse(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'overviewcourse/existsubscription/' + id);
  }

  public subscribeToCourse(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'overviewcourse/subscribetoccurse/' + id);
  }

  public evaluateCourse(idCourse: number, evaluation: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'overviewcourse/evaluate/'
      + idCourse
      + '/'
      + evaluation);
  }

  public gatEvaluate(idCourse: number): Observable<number> {
    return this.http.get<number>(this.userUrl + 'overviewcourse/getevaluation/' + idCourse);
  }

  public getCurrentUserEvaluation(idCourse: number): Observable<number> {
    return this.http.get<number>(this.userUrl + 'overviewcourse/getcurrentuserevaluation/' + idCourse);
  }

}
