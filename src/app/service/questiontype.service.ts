import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';
import {Test} from '../model/test.model';
import {Answer} from '../model/answer.model';
import {QuestionType} from '../model/questiontype.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionTypeService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getQuestionTypes(): Observable<QuestionType[]> {
    return this.http.get<QuestionType[]>(this.userUrl + 'question/allquestiontype');
  }

}
