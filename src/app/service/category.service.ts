import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../model/category.model';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.userUrl + 'category/all');
  }

}
