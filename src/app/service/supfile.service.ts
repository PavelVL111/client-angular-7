import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SupFileModel} from '../model/supfile-model';

@Injectable({
  providedIn: 'root'
})
export class SupFileService {
  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getSupFileById(id: number): Observable<SupFileModel> {
    return this.http.get<SupFileModel>(this.userUrl + 'supfile/get/' + id);
  }

  public getSupFilesBySetionId(id: number): Observable<SupFileModel[]> {
    return this.http.get<SupFileModel[]>(this.userUrl + 'supfile/get/all/section/' + id);
  }

  public deleteSupFileById(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.userUrl + 'supfile/delete/' + id);
  }

}
