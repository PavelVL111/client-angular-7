import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Course} from '../model/course-model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CoursesService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'courses/allcourses');
  }

  public getCoursesByName(name: string): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'courses/allcoursesbyname/' + name);
  }

  public getCourse(id: string): Observable<Course> {
    return this.http.get<Course>(this.userUrl + 'courses/course/' + id);
  }


  public deleteCourse(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'create/deletecourse/' + id);
  }


  public getCoursesByCategory(id: number): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'courses/coursesbycategory/' + id);
  }

  public getCoursesByTag(tag: string): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'courses/findbytag/' + tag);
  }

  public agreeCourse(id: number): Observable<any> {
    return this.http.get<any>(this.userUrl + 'courses/agree/' + id, {
      observe: 'response'
    });
  }

}
