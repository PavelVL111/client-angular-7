import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';
import {Test} from '../model/test.model';
import {Answer} from '../model/answer.model';
import {CommentCourseModel} from '../model/comment-course.model';

@Injectable({
  providedIn: 'root'
})
export class CommentCourseService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getCommentCourseModelsByCourseIdAndRequest(courseId: number): Observable<CommentCourseModel[]> {
    return this.http.get<CommentCourseModel[]>(this.userUrl + 'comment/course/all/' + courseId);
  }

  public createCommentCourseModel(commentCourseModel: CommentCourseModel): Observable<CommentCourseModel> {
    return this.http.post<CommentCourseModel>(this.userUrl + 'comment/course/save', commentCourseModel);
  }

  public deleteCommentCourseModel(commentCourseId: number): Observable<boolean> {
    return this.http.delete<boolean>(this.userUrl + 'comment/course/delete/' + commentCourseId);
  }

  public updateCommentCourseModel(commentCourseModel: CommentCourseModel): Observable<CommentCourseModel> {
    return this.http.post<CommentCourseModel>(this.userUrl + 'comment/course/save', commentCourseModel);
  }
}
