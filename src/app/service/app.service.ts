import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../model/user.model';
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {TokenStorage} from '../core/token.storage';
import {Router} from '@angular/router';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  constructor(private http: HttpClient, private router: Router) {}

  private userUrl = 'http://localhost:8080/';
  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl + 'users').pipe(
      catchError(this.handleError)
    );
  }

  public getUser(): Observable<User> {
    return this.http.get<User>(this.userUrl + 'user');
  }


  public getUserByID(userId: number): Observable<User> {
    return this.http.get<User>(this.userUrl + 'user/' + userId);
  }

  public updateUser(user: User): Observable<boolean> {
    return this.http.post<boolean>(this.userUrl + 'updateuser', user);
  }

  private handleError(error: HttpErrorResponse) {
     // error.error.status;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      this.router.navigate(['/login']);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
    this.router.navigate(['/login']);
  }
}
