import {Injectable} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Tag} from '../model/tag.model';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getTagsByIdCourse(idCourse: number): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.userUrl + 'tags/get/' + idCourse);
  }

  public saveTag(tag: Tag): Observable<Tag> {
    return this.http.post<Tag>(this.userUrl + 'tags/save', tag);
  }

  public deleteTagsByIdCourse(idCourse: number): Observable<boolean> {
    return this.http.delete<boolean>(this.userUrl + 'tags/delete/' + idCourse);
  }

  public deleteTag(idTag: number): Observable<boolean> {
    return this.http.delete<boolean>(this.userUrl + 'tags/delete/one/' + idTag);
  }

}
