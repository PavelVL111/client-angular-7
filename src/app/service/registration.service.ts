import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {RegistrationModel} from '../model/registration.model';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient, private router: Router) {}

  private userUrl = 'http://localhost:8080/';

  signUp(registrationModel: RegistrationModel): Observable<any> {
    const credentials = {mail: registrationModel.mail,
      password: registrationModel.password,
      passwordAgain: registrationModel.passwordAgain};
    return this.http.post<any>('http://localhost:8080/registration', credentials);
  }
}
