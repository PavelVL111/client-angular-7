import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Video} from '../model/video-model';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getVideosBySetion(id: number): Observable<Video[]> {
    return this.http.get<Video[]>(this.userUrl + 'video/allvideo/' + id);
  }

  public getVideo(id: number): Observable<Video> {
    return this.http.get<Video>(this.userUrl + 'video/video/' + id);
  }

  public deleteVideo(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'create/deletevideo/' + id);
  }
}
