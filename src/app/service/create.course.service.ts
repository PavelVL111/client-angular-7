import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Section} from '../model/section-model';
import {Video} from '../model/video-model';
import {Category} from '../model/category.model';
import {CourseCategory} from '../model/course.category.model';

@Injectable({
  providedIn: 'root'
})
export class CreateCourseService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getCoursesByName(name: string): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'create/allcoursesbyname/' + name);
  }

  public createCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(this.userUrl + 'create/course', course);
  }

  public updateCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(this.userUrl + 'create/updatecourse', course);
  }

  public createSection(section: any, id: number): Observable<any> {
    return this.http.get<Section>(this.userUrl + 'create/sections' + id, section);
  }

  public createVideo(video: any, id: number): Observable<any> {
    return this.http.get<Video>(this.userUrl + 'create/video' + id, video);
  }

  public getCoursesByCreateing(): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'create/coursesbycreateing');
  }

  public getCourseCategorys(): Observable<Category[]> {
    return this.http.get<Category[]>(this.userUrl + 'category/all');
  }

  public getCourseCategorysByidCourse(idCourse: number): Observable<CourseCategory[]> {
    return this.http.get<CourseCategory[]>(this.userUrl + 'create/getcategorybycourse/' + idCourse);
  }

  public setCourseCategory(idCourse: number, idCategory: number): Observable<CourseCategory> {
    return this.http.get<CourseCategory>(this.userUrl + 'create/coursecategory/' + idCourse + '/' + idCategory);
  }

  public deleteCourseCategory(courseCategory: CourseCategory): Observable<boolean> {
    return this.http.post<boolean>(this.userUrl + 'create/deletecoursecategory', courseCategory);
  }

}
