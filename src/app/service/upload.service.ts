import { Injectable } from '@angular/core';
import {HttpClient, HttpEventType, HttpResponse} from '@angular/common/http';
import {DataService} from '../core/data.service';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  public percentDone: number;
  public uploadSuccess: boolean;
  public uploadSuccessObservable = new Subject<boolean>();

  constructor( private http: HttpClient,
               private dataService: DataService
  ) { }

  uploadFile(files: File[], uploadRef: string) {
    const formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));

    this.http.post(uploadRef, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.dataService.setPercentDone(Math.round(100 * event.loaded / event.total));
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
          this.dataService.setPercentDone(-1);
          this.uploadSuccessObservable.next(true);
        }
      });
  }

  skipeUploadSuccessObservable() {
    this.uploadSuccessObservable.next(false);
  }

}
