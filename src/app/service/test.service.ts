import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Course} from '../model/course-model';
import {Blog} from '../model/blog.model';
import {Test} from '../model/test.model';
import {Section} from '../model/section-model';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) {}

  private userUrl = 'http://localhost:8080/';

  public getTests(id: number): Observable<Test[]> {
    return this.http.get<Test[]>(this.userUrl + 'test/alltests/' + id);
  }

  public getTest(id: number): Observable<Test> {
    return this.http.get<Test>(this.userUrl + 'test/test/' + id);
  }

  public createTest(id: number): Observable<Test> {
    return this.http.get<Test>(this.userUrl + 'create/createtest/' + id);
  }

  public deleteTest(id: number): Observable<Test> {
    return this.http.get<Test>(this.userUrl + 'create/deletetest/' + id);
  }

  public updateTest(test: Test): Observable<boolean> {
    return this.http.post<boolean>(this.userUrl + 'create/updatetest', test);
  }


}
