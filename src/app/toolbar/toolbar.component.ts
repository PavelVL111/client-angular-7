import {Component, DoCheck, OnInit} from '@angular/core';
import {MatButtonToggle, MatButtonToggleGroup, MatInputModule} from '@angular/material';
import {TokenStorage} from '../core/token.storage';
import {Interceptor} from '../core/inteceptor';
import {Router} from '@angular/router';
import {StatusObject} from '../core/status.object';
import {AuthCookie} from '../core/auth.cookie';
import {AuthorizationLogerService} from '../service/authorization.loger.service';
import {AuthorizationLoger} from '../core/authorization.loger';
import {DataService} from '../core/data.service';
import {Category} from '../model/category.model';
import {CategoryService} from '../service/category.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  flag = true;
  matInputModule: HTMLInputElement;
  isLogin: boolean;
  categories: Category[];
  constructor(private statusObject: StatusObject,
              private authCookie: AuthCookie,
              private tokenStorage: TokenStorage,
              private authorizationLoger: AuthorizationLoger,
              private authorizationLogerService: AuthorizationLogerService,
              private dataService: DataService,
              private router: Router,
              private categoryService: CategoryService
              ) { }

  ngOnInit() {
    this.getCategories();
    this.matInputModule = <HTMLInputElement>document.getElementById('search');
    this.authorizationLogerService.getAuthorizationStatus().subscribe(
      data => {
        this.isLogin = (<boolean>data);
      }
    );
    this.statusObject.setIsAuthorized(this.isLogin);
  }

  getCategories() {
    this.categoryService.getCategories().subscribe(data => {
      this.categories = data;
    });
  }

  onClick() {
    if (this.flag) {
      this.matInputModule.value = '';
      this.flag = false;
    }
  }

  onChange() {
    if (this.matInputModule.value === '') {
    } else {
    }
  }

  onBlur() {
    if (this.matInputModule.value === '') {
      this.matInputModule.value = 'Поиск';
      this.flag = true;
    }
  }

  onCancel() {
    this.matInputModule.value = 'Поиск';
  }

  exit() {
      this.authCookie.deleteAuth();
      this.tokenStorage.delToken();
    this.statusObject.setIsAuthorized(false);
  }

  login() {
    // this.isLogin = true;
  }

  onKeydown(event) {
    if (event.key === 'Enter') {
      this.dataService.setDataSearch(event.target.value);
      this.router.navigate(['search']);
    }
  }

  onClickCategory(id: number) {
    this.dataService.setCategory(id);
    this.router.navigate(['category/' + id]);
  }

  setErrorMassage() {
    this.dataService.setCodeRespond(0);
  }
}
