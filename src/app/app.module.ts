import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatDatepicker,
  MatDatepickerInput,
  MatDatepickerModule,
  MatFormFieldModule, MatIconRegistry, MatInputModule,
  MatNativeDateModule, MatNestedTreeNode,
  MatSliderModule, MatTree, MatTreeModule, MatTreeNode, MatTreeNodeDef, MatTreeNodeToggle
} from '@angular/material';
import {AppRoutingModule} from './core/app.routing.module';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomMaterialModule} from './core/material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './view/login/login.component';
import {ErrorDialogComponent} from './core/error-dialog.component';
import {UserComponent} from './other/user/user.component';
import {AuthRequestOptions} from './core/auth.request.options';
import {RequestOptions} from '@angular/http';
import {Interceptor} from './core/inteceptor';
import {AuthCookie} from './core/auth.cookie';
import {TokenStorage} from './core/token.storage';
import {AuthService} from './core/auth.service';
import {UserService} from './service/app.service';
import {PlayerComponent} from './other/player/player.component';
import { UploadComponent } from './other/upload/upload.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { HomeComponent } from './view/general/home/home.component';
import { CoursesCardsComponent } from './view/general/courses-cards/courses-cards.component';
import {CoursesService} from './service/courses.service';
import {StatusObject} from './core/status.object';
import {SentryErrorHandler} from './core/sentry.error.handler';
import {Router, RouterModule} from '@angular/router';
import {AuthorizationLogerService} from './service/authorization.loger.service';
import {AuthorizationLoger} from './core/authorization.loger';
import { CourseCardComponent } from './view/general/course-card/course-card.component';
import { PersonalAreaComponent } from './view/personal-area/personal-area.component';
import { RegistrationComponent } from './view/registration/registration.component';
import {RegistrationService} from './service/registration.service';
import {RegistrationModel} from './model/registration.model';
import {SuccesfullRegistrationComponent} from './view/succesfull-registration/succesfull.registration.component';
import { CourceLineCardComponent } from './view/personal-area/cource-line-card/cource-line-card.component';
import {CourseBySubscribeUserService} from './view/personal-area/course.by.subscribe.user';
import { OwerviewCourseComponent } from './view/owerview-course/owerview-course.component';
import {DataService} from './core/data.service';
import { CardOverviewComponent } from './view/owerview-course/card-overview/card-overview.component';
import {SubscribeCoursesService} from './service/subscribe.courses.service';
import { PersonalProfileComponent } from './view/personal-area/personal-profile/personal-profile.component';
import { UploadPhotoComponent } from './view/personal-area/personal-profile/upload-photo/upload-photo.component';
import { CourseContentComponent } from './view/course-content/course-content.component';
import { CourseContentCardComponent } from './view/course-content/course-content-card/course-content-card.component';
import { VideoListComponent } from './view/course-content/course-content-card/video-list/video-list.component';
import { VideoPlayerComponent } from './view/course-content/course-content-card/video-player/video-player.component';
import { SearchViewComponent } from './view/search-view/search-view.component';
import { CategoryViewComponent } from './view/category-view/category-view.component';
import { OtherPageComponent } from './other/other-page/other-page.component';
import { HelpComponent } from './view/help/help.component';
import { AboutProjectComponent } from './view/about-project/about-project.component';
import { BlogComponent } from './view/blog/blog.component';
import { PostCardComponent } from './view/blog/post-card/post-card.component';
import { PostViewComponent } from './view/post-view/post-view.component';
import { CardPostViewComponent } from './view/post-view/card-post-view/card-post-view.component';
import { TeachViewComponent } from './view/personal-area/teach-view/teach-view.component';
import {CreateCourseComponent} from './view/create-course-component/create-course-component.component';
import { CreateCourseCardComponent } from './view/personal-area/teach-view/create-course-card/create-course-card.component';
import { UploadListVideoComponent } from './view/create-course-component/create-section/upload-list-video/upload-list-video.component';
import { CategoryComponent } from './view/create-course-component/create-owerview/category/category.component';
import { NewloginComponent } from './view/login/newlogin/newlogin.component';
import { CreateOwerviewComponent } from './view/create-course-component/create-owerview/create-owerview.component';
import { CreateSectionComponent } from './view/create-course-component/create-section/create-section.component';
import {CreateTestComponent} from './view/create-course-component/create-test/create-test.component';
import { CreateCertificateComponent } from './view/create-course-component/create-certificate/create-certificate.component';
import { CreateTestFormComponent } from './view/create-course-component/create-test/create-test-form/create-test-form.component';
import { TestsButtonLineComponent } from './view/create-course-component/create-test/tests-button-line/tests-button-line.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CreateQuestionComponent } from './view/create-course-component/create-test/create-test-form/create-question/create-question.component';
import { CreateAnswerComponent } from './view/create-course-component/create-test/create-test-form/create-question/create-answer/create-answer.component';
import { TagsComponent } from './view/create-course-component/create-owerview/tags/tags.component';
import { UploadListSupfileComponent } from './view/create-course-component/create-section/upload-list-supfile/upload-list-supfile.component';
import { TestComponent } from './view/test/test.component';
import { QuestionTestComponent } from './view/test/question-test/question-test.component';
import { TestResultComponent } from './view/test-result/test-result.component';
import { TestItemComponent } from './view/course-content/course-content-card/test-item/test-item.component';
import { PdfViewComponent } from './view/course-content/course-content-card/pdf-view/pdf-view.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { CertificateViewComponent } from './view/certificate-view/certificate-view.component';
import {CommentCourseService} from './service/comment.course.service';
import { CommentCardComponent } from './view/owerview-course/comment-card/comment-card.component';
import { CommentItemComponent } from './view/owerview-course/comment-card/comment-item/comment-item.component';
import { VideoListOverviewComponent } from './view/owerview-course/card-overview/video-list-overview/video-list-overview.component';
import { TestListOverviewComponent } from './view/owerview-course/card-overview/test-list-overview/test-list-overview.component';
import { FileListOverviewComponent } from './view/owerview-course/card-overview/file-list-overview/file-list-overview.component';
import { DownPanelComponent } from './down-panel/down-panel.component';
import { CoursesByTagComponent } from './view/courses-by-tag/courses-by-tag.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginComponent,
    ErrorDialogComponent,
    PlayerComponent,
    UploadComponent,
    ToolbarComponent,
    HomeComponent,
    CoursesCardsComponent,
    CourseCardComponent,
    PersonalAreaComponent,
    RegistrationComponent,
    SuccesfullRegistrationComponent,
    CourceLineCardComponent,
    OwerviewCourseComponent,
    CardOverviewComponent,
    PersonalProfileComponent,
    UploadPhotoComponent,
    CourseContentComponent,
    CourseContentCardComponent,
    VideoListComponent,
    VideoPlayerComponent,
    SearchViewComponent,
    CategoryViewComponent,
    OtherPageComponent,
    HelpComponent,
    AboutProjectComponent,
    BlogComponent,
    PostCardComponent,
    PostViewComponent,
    CardPostViewComponent,
    TeachViewComponent,
    CreateCourseComponent,
    CreateCourseCardComponent,
    UploadListVideoComponent,
    CategoryComponent,
    NewloginComponent,
    CreateOwerviewComponent,
    CreateSectionComponent,
    CreateTestComponent,
    CreateCertificateComponent,
    CreateTestFormComponent,
    TestsButtonLineComponent,
    CreateQuestionComponent,
    CreateAnswerComponent,
    TagsComponent,
    UploadListSupfileComponent,
    TestComponent,
    QuestionTestComponent,
    TestResultComponent,
    TestItemComponent,
    PdfViewComponent,
    CertificateViewComponent,
    CommentCardComponent,
    CommentItemComponent,
    VideoListOverviewComponent,
    TestListOverviewComponent,
    FileListOverviewComponent,
    DownPanelComponent,
    CoursesByTagComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ScrollingModule,
    PdfViewerModule,
  ],
  exports: [],
  entryComponents: [ErrorDialogComponent, CreateTestFormComponent, PdfViewComponent],
  providers: [ErrorDialogComponent, UserService, AuthService, TokenStorage, TokenStorage, AuthCookie, StatusObject, CoursesService,
    AuthorizationLogerService, AuthorizationLoger, RegistrationService, RegistrationModel, SuccesfullRegistrationComponent,
    CourseBySubscribeUserService, DataService, SubscribeCoursesService, CommentCourseService, FormBuilder, MatIconRegistry,
    {provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi : true},
    {
      provide: RequestOptions,
      useClass: AuthRequestOptions
    },
    { provide: ErrorHandler, useClass: SentryErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
