import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';

declare var videojs: any;

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, AfterViewInit, OnDestroy {
  private videoJSplayer: any;
  src = 'http://localhost:8080/video/1.mp4';
  videoElement: HTMLVideoElement;
  source: any;
  constructor() {
  }

  ngOnInit() {
    this.source = document.getElementById('idsrc');
    // this.videoElement = <HTMLVideoElement>document.getElementsByTagName('video')[0];
    // this.videoElement.src = 'http://localhost:8080/video/Pre_Int_02_90815.mp4';
  }

  ngAfterViewInit(): void {
    this.initVideoJs();
  }

  initVideoJs() {
    this.videoJSplayer = videojs('video_player');
    // const transcript = this.videoJSplayer.transcript();
    // const transcriptCon = document.querySelector('#transcriptContainer');
    // transcriptCon.appendChild(transcript.el());
  }

  ngOnDestroy() {
    this.videoJSplayer.dispose();
  }

  onClickMe() {
    this.videoJSplayer.pause();
    this.source.setAttribute('src', 'http://localhost:8080/videop/7.mp4');
    this.src = 'http://localhost:8080/video/10.mp4';
    this.videoJSplayer.src(this.src);
    this.videoJSplayer.load('http://localhost:8080/video/7.mp4');
    this.videoJSplayer.play();
  }

  onClickMe2() {
    this.videoJSplayer.pause();
    this.src = 'http://localhost:8080/video/2.mp4';
    this.source.setAttribute('src', this.src);
    this.videoJSplayer.src(this.src);
    this.videoJSplayer.load(this.src);
    this.videoJSplayer.play();
  }
  //
  // onClickMe2() {
  //   this.videoJSplayer.pause();
  //   this.src = 'http://localhost:8080/video/2.pm4';
  //   this.videoJSplayer.src(this.src);
  //   this.videoJSplayer.load();
  //   this.videoJSplayer.play();
  // }

}
