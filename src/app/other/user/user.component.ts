import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {User} from '../../model/user.model';
import {UserService} from '../../service/app.service';
import {Router} from '@angular/router';
import {StatusObject} from '../../core/status.object';
import {DataService} from '../../core/data.service';

type StarValue = Array<{ id: number}>;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  numberStar: StarValue = [
    {id: 1},
    {id: 2},
    {id: 3},
    {id: 4},
    {id: 5},
  ];

  displayedColumns = ['id', 'username', 'salary', 'age'];
  dataSource = new MatTableDataSource<User>();
  constructor(private router: Router, private userService: UserService, private statusObject:
    StatusObject, private dataService: DataService) {
  }
  ngOnInit(): void {
    this.dataService.setRefUpload('http://localhost:8080/video');
    this.userService.getUsers().subscribe(
      data => {
        this.dataSource.data = data;
      }
    );
  }

  addElement() {
    this.numberStar.push({id: this.numberStar.length + 1});
  }

}

