import { Component, VERSION } from '@angular/core';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType} from '@angular/common/http';
import {DataService} from '../../core/data.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent {
  percentDone: number;
  uploadSuccess: boolean;
  inputElement: HTMLInputElement;
  constructor(
    private http: HttpClient,
    private dataService: DataService
  ) { }

  version = VERSION;

  upload(files: File[]) {
    this.uploadAndProgress(files);
  }

  basicUpload(files: File[]) {
    const formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));
    this.http.post(this.dataService.currentRefUpload, formData)
      .subscribe(event => {
        console.log('done');
      });
  }
  basicUploadSingle(file: File) {
    this.http.post(this.dataService.currentRefUpload, file)
      .subscribe(event => {
        console.log('done');
      });
  }

  uploadAndProgress(files: File[]) {
    const formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));

    this.http.post(this.dataService.currentRefUpload, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
      });
  }

  uploadAndProgressSingle(file: File) {
    this.http.post(this.dataService.currentRefUpload, file, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
      });
  }

  onClick() {
   this.inputElement = <HTMLInputElement>document.getElementById('openFile');
   this.inputElement.click();
  }
}
