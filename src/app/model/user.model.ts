export interface User {
  id: number;
  nikname: string;
  mail: string;
  fullname: string;
  avatar: string;
  roleId: number;
  dateRegistration: Date;
  birthday: Date;
}
