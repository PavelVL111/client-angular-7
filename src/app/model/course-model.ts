export interface Course {
  id: number;
  name: string;
  open: boolean;
  overview: string;
  confirmed: boolean;
  tags: string;
  img: string;
  dateCreation: Date;
  idUser: number;
  categoryId: number;
  agree: boolean;
}
