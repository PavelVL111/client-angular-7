export interface CourseCategory {
  id: number;
  courseId: number;
  categoryId: number;
}
