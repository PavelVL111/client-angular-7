export interface Blog {
  id: number;
  name: string;
  overview: string;
  datePost: Date;
  img: string;
}
