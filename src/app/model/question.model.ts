export interface Question {
  id: number;
  text: string;
  testId: number;
  questionType: number;
}
