export interface Section {
  id: number;
  header: string;
  owerview: string;
  courseId: number;
}
