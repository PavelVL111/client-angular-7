export interface Answer {
  id: number;
  text: string;
  right: boolean;
  sectionId: number;
}
