export interface Tag {
  id: number;
  tag: string;
  courseId: number;
}
