export interface CommentCourseModel {
  id: number;
  text: string;
  userId: number;
  courseId: number;
  date: Date;
}
