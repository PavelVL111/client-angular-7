export interface SupFileModel {
  id: number;
  ref: string;
  sectionId: number;
  name: string;
}
