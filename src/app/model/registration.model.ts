import {Injectable} from '@angular/core';

@Injectable()
export class RegistrationModel {

  private _mail: string;
  private _password: string;
  private _passwordAgain: string;

  constructor() {}


  get mail(): string {
    return this._mail;
  }

  set mail(value: string) {
    this._mail = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get passwordAgain(): string {
    return this._passwordAgain;
  }

  set passwordAgain(value: string) {
    this._passwordAgain = value;
  }

  // getIsAuthorized(): boolean {
  //   return this.isAuthorized;
  // }
  //
  // setIsAuthorized (value: boolean) {
  //   this.isAuthorized = value;
  // }
}

