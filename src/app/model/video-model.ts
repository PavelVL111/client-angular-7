export interface Video {
  id: number;
  ref: string;
  sectionId: number;
  name: string;
}
