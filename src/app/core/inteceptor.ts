import { Injectable } from '@angular/core';
import {
  HttpInterceptor, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent,
  HttpResponse, HttpUserEvent, HttpErrorResponse, HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {TokenStorage} from './token.storage';
import 'rxjs/add/operator/do';
import {StatusObject} from './status.object';
import {AuthCookie} from './auth.cookie';
import {DataService} from './data.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {
  getHandler: any;
  constructor(private token: TokenStorage,
              private router: Router,
              private statusObject: StatusObject,
              private dataService: DataService,
              private authCookie: AuthCookie) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    let authReq = req;
    if (this.token.getToken() != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer' + this.token.getToken())});
    }
    // this.getHandler = next.handle(authReq).do(
    //   (err: any) => {
    //     if (err instanceof HttpErrorResponse) {
    //
    //
    //       if (err.status === 401) {
    //         this.router.navigate(['']);
    //       }
    //     }
    //     if (err.status === 401) {
    //     }
    //     if (err.status === 200) {
    //       console.log();
    //     }
    //   }
    // );


    return next.handle(authReq).do(evt => {
      if (evt instanceof HttpResponse) {
        if (evt.status === 401) {
          this.dataService.setCodeRespond(401);
                console.log(evt.status, 'my');
              }
        if (evt.status === 200) {
          console.log(evt.status, 'my');
          if (authReq.url === 'http://localhost:8080/token/generate-token') {
            this.dataService.setCodeRespond(200);
            this.statusObject.setIsAuthorized(true);
          }
        }
      }
    });
    // return this.getHandler;

  }

}
