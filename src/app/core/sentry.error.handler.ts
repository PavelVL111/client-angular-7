import {ErrorHandler, Inject, Injectable, NgModule} from '@angular/core';
import {StatusObject} from './status.object';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {DataService} from './data.service';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  num: number;
  constructor(
    private statusObject: StatusObject,
    private dataService: DataService
  ) {  }
  handleError(err: any): void {
    if (err instanceof HttpErrorResponse) {
      if (err.status === 401) {
        // this.router.navigate(['']);
        this.dataService.setHttpErrorResponse(err);
        console.log(err.message, 'e');
        this.dataService.setCodeRespond(401);
      }
    }
    this.statusObject.setIsAuthorized(false);
  }
}
