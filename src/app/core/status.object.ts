import {Injectable} from '@angular/core';

@Injectable()
export class StatusObject {
  private isAuthorized: boolean;
  constructor() {this.isAuthorized = false; }


  getIsAuthorized(): boolean {
    return this.isAuthorized;
  }

  setIsAuthorized (value: boolean) {
    this.isAuthorized = value;
  }
}
