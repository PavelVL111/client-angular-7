import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTreeModule} from '@angular/material/tree';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatInputModule,
  MatTableModule,
  MatToolbarModule,
  MatIconModule,
  MatDivider,
  MatDividerModule,
  MatListModule,
  MatListItem,
  MatSidenavModule,
  MatGridListModule,
  MatTabsModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatSliderModule,
  MatProgressSpinnerModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatStepperModule,
  MatMenuModule,
  MatExpansionModule,
  MatSlideToggleModule,
  MatSelectModule,
  MatChipsModule, MatProgressBarModule, MatRadioModule, MatSortModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorDialogComponent} from './error-dialog.component';

@NgModule({
  imports: [CommonModule, MatToolbarModule, MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatTableModule, MatIconModule,
    MatDividerModule, MatListModule, MatSidenavModule, MatGridListModule, MatTabsModule,  MatDatepickerModule, MatFormFieldModule,
    MatNativeDateModule, MatInputModule, MatSliderModule, MatProgressSpinnerModule, MatButtonModule, MatButtonToggleModule, MatTreeModule,
    MatIconModule, MatStepperModule, MatMenuModule, MatExpansionModule, MatSlideToggleModule, MatSelectModule, MatChipsModule,
    MatProgressBarModule, MatRadioModule, MatCheckboxModule, MatSortModule

  ],

  exports: [CommonModule, MatToolbarModule, MatButtonModule, MatCardModule, MatInputModule, MatDialogModule, MatTableModule, MatIconModule,
    MatDividerModule, MatListModule, MatSidenavModule, MatGridListModule, MatTabsModule, MatDatepickerModule, MatFormFieldModule,
    MatNativeDateModule, MatInputModule, BrowserAnimationsModule, MatSliderModule, MatProgressSpinnerModule, MatButtonModule,
    MatButtonToggleModule, MatTreeModule, MatIconModule, MatStepperModule, MatMenuModule, MatExpansionModule, MatSlideToggleModule,
    MatSelectModule, MatChipsModule, MatProgressBarModule, MatRadioModule, MatCheckboxModule, MatSortModule

  ],
  providers: [MatDatepickerModule],
})
export class CustomMaterialModule { }
