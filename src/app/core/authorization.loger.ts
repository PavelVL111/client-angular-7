import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../model/user.model';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AuthorizationLogerService} from '../service/authorization.loger.service';

@Injectable()
export class AuthorizationLoger {

  private isAuthorization: boolean;
  constructor(private authorizationLogerService: AuthorizationLogerService) { }

  getAuthorizationStatus() {
    this.authorizationLogerService.getAuthorizationStatus().subscribe(
      data => {
        this.isAuthorization = (<boolean>data);
      }
    );
  }


  getIsAuthorization(): boolean {
    return this.isAuthorization;
  }
}
