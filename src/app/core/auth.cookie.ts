import { Injectable } from '@angular/core';
import {Cookie} from 'ng2-cookies/ng2-cookies';

const AUTH_HEADER_KEY = 'Authorization';
const AUTH_PREFIX = 'Bearer';

@Injectable()
export class AuthCookie {
  constructor() { }

  getAuth(): string {
    return Cookie.get(AUTH_HEADER_KEY);
  }

  setAuth(value: string): void {
    Cookie.set(AUTH_HEADER_KEY, AUTH_PREFIX + value, 1);
  }

  deleteAuth(): void {
    Cookie.deleteAll();
  }
}
