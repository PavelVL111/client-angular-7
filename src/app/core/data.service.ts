import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {Section} from '../model/section-model';

@Injectable()
export class DataService {

  private dataSource = new BehaviorSubject('default data');
  private refDataSource = new BehaviorSubject('-1');

  public dataSearch = new BehaviorSubject('');

  public category = new BehaviorSubject(0);

  public lastUploadSection = new BehaviorSubject(-1);

  public lastDeleteCourse = new BehaviorSubject(-1);

  public codeRespond = new BehaviorSubject(0);

  public httpErrorResponse: HttpErrorResponse;

  public currentData = this.dataSource.asObservable();

  public currentRefUpload: string;

  public currentRefVideo = this.refDataSource.asObservable();

  public currentCourseCategory = new BehaviorSubject(-1);

  public firstVideoRef = new BehaviorSubject('');

  public dataCourseCategory = this.currentCourseCategory.asObservable();

  public courseSections = new BehaviorSubject(null);

  public percentDone = new BehaviorSubject(-1);

  public files: File[];

  constructor() { }

  changeData(data: string) {
    this.dataSource.next(data);
  }

  setRefUpload(ref: string) {
    this.currentRefUpload = ref;
  }

  setRefVideo(ref: string) {
    this.refDataSource.next(ref);
  }

  setDataSearch(dataSearch: string) {
    this.dataSearch.next(dataSearch);
  }

  setCategory(id: number) {
    this.category.next(id);
  }

  setLastUploadSection(id: number) {
    this.lastUploadSection.next(id);
  }

  setLastDeleteCourse(id: number) {
    this.lastDeleteCourse.next(id);
  }

  setCodeRespond(code: number) {
    this.codeRespond.next(code);
  }

  setCourseCategory(code: number) {
    this.currentCourseCategory.next(code);
  }

  setHttpErrorResponse(httpErrorResponse: HttpErrorResponse) {
    this.httpErrorResponse = httpErrorResponse;
  }

  setCourseSections(sectionArray: Array<{id: number, value: Section}>) {
    this.courseSections.next(sectionArray);
  }

  setPercentDone(percentDone: number) {
    this.percentDone.next(percentDone);
  }

  setFirstVideoRef(firstVideoRef: string) {
    this.firstVideoRef.next(firstVideoRef);
  }

}
