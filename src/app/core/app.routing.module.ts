import { NgModule } from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {UserComponent} from '../other/user/user.component';
import {LoginComponent} from '../view/login/login.component';
import {HomeComponent} from '../view/general/home/home.component';
import {ErrorDialogComponent} from './error-dialog.component';
import {SentryErrorHandler} from './sentry.error.handler';
import {PersonalAreaComponent} from '../view/personal-area/personal-area.component';
import {RegistrationComponent} from '../view/registration/registration.component';
import {SuccesfullRegistrationComponent} from '../view/succesfull-registration/succesfull.registration.component';
import {OwerviewCourseComponent} from '../view/owerview-course/owerview-course.component';
import {CourseContentComponent} from '../view/course-content/course-content.component';
import {SearchViewComponent} from '../view/search-view/search-view.component';
import {CategoryViewComponent} from '../view/category-view/category-view.component';
import {HelpComponent} from '../view/help/help.component';
import {AboutProjectComponent} from '../view/about-project/about-project.component';
import {BlogComponent} from '../view/blog/blog.component';
import {PostCardComponent} from '../view/blog/post-card/post-card.component';
import {PostViewComponent} from '../view/post-view/post-view.component';
import {CreateCourseComponent} from '../view/create-course-component/create-course-component.component';
import {TestComponent} from '../view/test/test.component';
import {TestResultComponent} from '../view/test-result/test-result.component';
import {CertificateViewComponent} from '../view/certificate-view/certificate-view.component';
import {CoursesByTagComponent} from '../view/courses-by-tag/courses-by-tag.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'user', component: UserComponent},
  {path: 'personalarea', component: PersonalAreaComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'succesfullregistration', component: SuccesfullRegistrationComponent},
  {path: 'owerviewcourse/:id', component: OwerviewCourseComponent},
  {path: 'coursecontent/:id', component: CourseContentComponent},
  {path: 'search', component: SearchViewComponent},
  {path: 'category/:id', component: CategoryViewComponent},
  {path: 'help', component: HelpComponent},
  {path: 'about', component: AboutProjectComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'post/:id', component: PostViewComponent},
  {path: 'create/:id', component: CreateCourseComponent},
  {path: 'test/:id', component: TestComponent},
  {path: 'testresoult/:id', component: TestResultComponent},
  {path: 'certificate/:id', component: CertificateViewComponent},
  {path: 'searchbytag/:id', component: CoursesByTagComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
