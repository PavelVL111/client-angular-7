import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  @Input() blog;
  constructor() { }

  ngOnInit() {
    this.blog.overview = this.htmlToPlaintext(this.blog.overview);
    this.blog.overview = this.blog.overview.substr(0, 310) + '...';
  }

  htmlToPlaintext(text: string): string {
    return text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }

}
