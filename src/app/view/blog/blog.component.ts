import { Component, OnInit } from '@angular/core';
import {Course} from '../../model/course-model';
import {Blog} from '../../model/blog.model';
import {BlogService} from '../../service/blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  blogs: Blog[];
  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.blogService.getBlogs().subscribe(data => {
      this.blogs = data;
    });
  }

}
