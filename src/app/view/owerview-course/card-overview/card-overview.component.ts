import {Component, OnInit} from '@angular/core';
import {Course} from '../../../model/course-model';
import {DataService} from '../../../core/data.service';
import {CoursesService} from '../../../service/courses.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SubscribeCoursesService} from '../../../service/subscribe.courses.service';
import {StatusObject} from '../../../core/status.object';
import {AuthorizationLoger} from '../../../core/authorization.loger';
import {AuthorizationLogerService} from '../../../service/authorization.loger.service';
import {Tag} from '../../../model/tag.model';
import {TagService} from '../../../service/tag.service';
import {Section} from '../../../model/section-model';
import {Video} from '../../../model/video-model';
import {Test} from '../../../model/test.model';
import {User} from '../../../model/user.model';
import {SectionService} from '../../../service/section.service';
import {VideoService} from '../../../service/video.service';
import {UserService} from '../../../service/app.service';
import {TestService} from '../../../service/test.service';

type StarValue = Array<{ id: number, name: string }>;

@Component({
  selector: 'app-card-overview',
  templateUrl: './card-overview.component.html',
  styleUrls: ['./card-overview.component.css']
})
export class CardOverviewComponent implements OnInit {

  course: Course;
  sections: Section[];
  video: Video;
  test: Test;
  user: User;

  countVideos = {count: 0};
  countFiles = {count: 0};
  countTests = {count: 0};

  tags: Tag[];
  subscribeCourse: boolean;
  auth: boolean;
  evaluation: number;
  numberStar: StarValue = [
    {id: 1, name: 'star_border'},
    {id: 2, name: 'star_border'},
    {id: 3, name: 'star_border'},
    {id: 4, name: 'star_border'},
    {id: 5, name: 'star_border'},
  ];

  constructor(private dataService: DataService,
              private coursesService: CoursesService,
              private sectionService: SectionService,
              private testService: TestService,
              private userService: UserService,
              private route: ActivatedRoute,
              private authorizationLoger: AuthorizationLoger,
              private authorizationLogerService: AuthorizationLogerService,
              private router: Router,
              private tagService: TagService,
              private subscribeCoursesService: SubscribeCoursesService) {
  }

  ngOnInit() {
    this.isAuth();
    this.coursesService.getCourse(this.route.snapshot.params['id']).subscribe(
      data => {
        this.course = data;
        this.isSubscribeCourse(this.course.id);
        this.getEvaluation();
        this.initTags();
        this.initSection();
      }
    );
  }

  initSection() {
    this.sectionService.getSectionsByCourse(this.course.id).subscribe(section => {
      this.sections = section;
    });
  }

  //
  // initVideo() {
  //
  // }
  //
  // initTest() {
  //   this.testService.getTests(this.section.id).subscribe();
  // }
  //
  // initUser() {
  //   this.userService.getUserByID(this.course.idUser).subscribe()
  // }
  //

  initTags() {
    this.tagService.getTagsByIdCourse(this.course.id).subscribe(tags => {
      this.tags = tags;
    });
  }

  isSubscribeCourse(id: number) {
    this.subscribeCoursesService.isSubscribeCourse(id).subscribe(
      data => {
        this.subscribeCourse = data;
      });
  }

  subscribeToCourse(id: number) {
    if (!this.auth) {
      this.router.navigate(['registration']);
    }
    this.subscribeCoursesService.subscribeToCourse(id).subscribe(
      data => {
        this.subscribeCourse = data;
      });
  }

  isAuth() {
    this.authorizationLogerService.getAuthorizationStatus().subscribe(
      data => {
        this.auth = (<boolean>data);
      }
    );
  }

  getEvaluation() {
    this.subscribeCoursesService.gatEvaluate(this.course.id).subscribe(data => {
      this.evaluation = data;
      this.evaluation = Math.ceil((this.evaluation) * 100) / 100;
      this.paintOverStar(data);
    });
  }

  paintOverStar(evaluation: number) {
    for (let i = 0; i <= evaluation - 1; i++) {
      this.numberStar[i].name = 'star';
    }
    if (Math.ceil(((evaluation < 1.0) ? evaluation : (evaluation % Math.floor(evaluation))) * 100) > 0) {
      this.numberStar[Math.floor(evaluation)].name = 'star_half';
    }
  }

}
