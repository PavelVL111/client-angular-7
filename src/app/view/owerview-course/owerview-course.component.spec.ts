import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwerviewCourseComponent } from './owerview-course.component';

describe('OwerviewCourseComponent', () => {
  let component: OwerviewCourseComponent;
  let fixture: ComponentFixture<OwerviewCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwerviewCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwerviewCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
