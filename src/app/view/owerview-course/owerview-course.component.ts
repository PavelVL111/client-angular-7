import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {CourseCardComponent} from '../general/course-card/course-card.component';
import {DataService} from '../../core/data.service';
import {CoursesService} from '../../service/courses.service';
import {Course} from '../../model/course-model';

@Component({
  selector: 'app-owerview-course',
  templateUrl: './owerview-course.component.html',
  styleUrls: ['./owerview-course.component.css']
})
export class OwerviewCourseComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }


}
