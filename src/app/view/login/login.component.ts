import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ErrorStateMatcher, MatDialog} from '@angular/material';
import {AuthService} from '../../core/auth.service';
import {TokenStorage} from '../../core/token.storage';
import {AuthCookie} from '../../core/auth.cookie';
import {HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {StatusObject} from '../../core/status.object';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {DataService} from '../../core/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              private token: TokenStorage,
              private authCookie: AuthCookie,
              private dataService: DataService
  ) {
  }

  username: string;
  password: string;
  hasError = false;
  showProgress = false;
  htmlInputElement: HTMLInputElement;
  httpErrorResponse: HttpErrorResponse;

  emailFormControl = new FormControl('', [
    Validators.required,
  ]);

  ngOnInit(): void {
    this.htmlInputElement = <HTMLInputElement> document.getElementsByClassName('input-name')[0];
      this.dataService.codeRespond.subscribe(data => {
      if (data === 401) {
        this.hasError = true;
        this.httpErrorResponse = this.dataService.httpErrorResponse;
        this.emailFormControl.setErrors(this.emailFormControl);
        this.setProgresFalse();
        this.htmlInputElement.click();
      } else {
        this.hasError = false;
      }
    });
  }

  login(): void {
    this.hasError = false;
    this.setProgresTrue();
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);

        this.authCookie.setAuth(data.token);
        this.router.navigate(['personalarea']);
      }
    );
  }

  setProgresTrue(): void {
    this.showProgress = true;
  }

  setProgresFalse(): void {
    this.showProgress = false;
    console.log();
  }

  onKeydown(event) {
    if (event.key === 'Enter') {
      this.login();
    }
  }

}
