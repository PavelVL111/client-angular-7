import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../../../../model/video-model';
import {VideoService} from '../../../../service/video.service';
import {DataService} from '../../../../core/data.service';

@Component({
  selector: 'app-upload-list-video',
  templateUrl: './upload-list-video.component.html',
  styleUrls: ['./upload-list-video.component.css']
})
export class UploadListVideoComponent implements OnInit {
  @Input() id;
  videos: Video[];
  displayedColumns: string[] = ['name', 'delete'];

  constructor(private videoService: VideoService,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.getVideos();
    this.dataService.lastUploadSection.subscribe(data => {
      if (data === this.id) {
        this.getVideos();
      }
    });
    // this.videoService.getVideosBySetion(this.id).subscribe(data => {
    //   this.video = data;
    // });
  }

  getVideos() {
    this.videoService.getVideosBySetion(this.id).subscribe(data => {
      this.videos = data;
    });
  }

  onClick(id: number) {
    this.videoService.deleteVideo(id).subscribe(data => {
        if (data) {
          this.dataService.setLastUploadSection(this.id);
        }
      }
    );
  }

}
