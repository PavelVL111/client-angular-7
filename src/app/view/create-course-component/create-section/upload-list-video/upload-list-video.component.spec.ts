import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadListVideoComponent } from './upload-list-video.component';

describe('UploadListVideoComponent', () => {
  let component: UploadListVideoComponent;
  let fixture: ComponentFixture<UploadListVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadListVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadListVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
