import {Component, Input, OnInit} from '@angular/core';
import {Section} from '../../../model/section-model';
import {Course} from '../../../model/course-model';
import {Video} from '../../../model/video-model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {VideoService} from '../../../service/video.service';
import {SectionService} from '../../../service/section.service';
import {CoursesService} from '../../../service/courses.service';
import {CreateCourseService} from '../../../service/create.course.service';
import {UploadService} from '../../../service/upload.service';
import {DataService} from '../../../core/data.service';

type SectionArray = Array<{id: number, value: Section}>;

@Component({
  selector: 'app-create-section',
  templateUrl: './create-section.component.html',
  styleUrls: ['./create-section.component.css']
})
export class CreateSectionComponent implements OnInit {

  @Input()
  courseId: number;
  sections: SectionArray = [];
  videos: Video[][];
  sectionId;
  maxSectionId;
  currentSectionForUpload: number;
  private baseURl = 'http://localhost:8080/';
  nameImg: string;
  inputElement: HTMLInputElement;
  isUploadVideo: boolean;

  percentDone: number;
  uploadSuccess: boolean;

  formGroup: FormGroup;
  formGroup2: FormGroup;

  constructor(private route: ActivatedRoute,
              private videoService: VideoService,
              private sectionService: SectionService,
              private coursesService: CoursesService,
              private createCourseService: CreateCourseService,
              private uploadService: UploadService,
              private dataService: DataService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {

    this.formGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });


    this.formGroup2 = this._formBuilder.group({
      firstCtrl2: ['', Validators.required]
    });

    if (this.route.snapshot.params['id'] === 'add') {
      // this.createSection();
    } else {
      this.getSections(this.courseId);
    }
    this.indicatorUpload();
  }

  createSection() {
    this.sectionService.createSection(this.courseId).subscribe(data => {
      this.addElement(data);
    });
  }

  getSections(idCourse: number) {
    this.sectionService.getSectionsByCourse(idCourse).subscribe( sectionsByCourse => {
        if (sectionsByCourse.length > 0) {
          this.initSection(sectionsByCourse);
        } else {
          // this.createSection();
        }
      }
    );
  }

  addElement(section: Section) {
    this.sectionId = this.sections.map(eachSection => {
      return eachSection.id;
    });
    if (this.sections.length === 0) {
      this.sections.push({id: 1, value: section});
    } else {
      this.maxSectionId = this.sectionId.reduce((a, b) => Math.max(a, b));
      this.sections.push({id: this.maxSectionId + 1, value: section});
    }
    this.dataService.setCourseSections(this.sections);
  }


  deleteElement(value: number) {
    const idSection = this.sections.findIndex(sections => sections.id === value);
    this.sectionService.deleteSection(this.sections[idSection].value.id).subscribe(data => {
      if (data) {
        this.sections.splice(this.sections.findIndex(sections => sections.id === value), 1);
      }
    });
    this.dataService.setCourseSections(this.sections);
  }

  initSection(sections: Section[]) {
    for (let i = 0; i < sections.length; i++) {
      this.sections.push({id: i, value: sections[i]});
    }
    this.dataService.setCourseSections(this.sections);
    this.initVideos(sections);
  }

  initVideos(sections: Section[]) {
    for (let i = 0; i < sections.length; i++) {
      this.videoService.getVideosBySetion(sections[i].id).subscribe(data => {
        this.videos.push(data);
      });
    }
  }

  getVideosBySectionForView(sectionId: number): Video[] {
    for (let i = 0; i < this.videos.length; i++) {
      if (this.videos[i][0].sectionId === sectionId) {
        return this.videos[i];
      }
    }
  }

  onClickUploadVideo(id: number): void {
    this.currentSectionForUpload = id;
    this.inputElement = <HTMLInputElement>document.getElementById('openVideo' + id);
    this.inputElement.click();
  }

  onClickUploadSupFile(id: number): void {
    this.currentSectionForUpload = id;
    this.inputElement = <HTMLInputElement>document.getElementById('openFile' + id);
    this.inputElement.click();
  }

  setUploadVideo(files: File[]) {
    this.nameImg = files[0].name;
    this.uploadVideo(files);
  }

  setUploadSupFile(files: File[]) {
    this.nameImg = files[0].name;
    this.uploadSupFile(files);
  }

  uploadVideo(files: File[]) {
    this.uploadService.uploadFile(files, this.baseURl + 'create/uploadvideo/' + this.currentSectionForUpload);
    this.uploadService.uploadSuccessObservable.subscribe(value => {
      this.isUploadVideo = value;
      this.dataService.setLastUploadSection(this.currentSectionForUpload);
      this.inputElement.value = '';
    });
  }


  uploadSupFile(files: File[]) {
    this.uploadService.uploadFile(files, this.baseURl + 'supfile/upload/' + this.currentSectionForUpload);
    this.uploadService.uploadSuccessObservable.subscribe(value => {
      this.isUploadVideo = value;
      this.dataService.setLastUploadSection(this.currentSectionForUpload);
      this.inputElement.value = '';
    });
  }

  onBlurSectionHeader(event, idSection: number) {
    const sectionId = this.sections.findIndex(sections => sections.id === idSection);
    if (event.target.value !== this.sections[sectionId].value.header) {
      this.sections[sectionId].value.header = event.target.value;
      this.updateSection(this.sections[sectionId].value, sectionId);
    }
  }

  onBlurSectionOverview(event, idSection: number) {
    const sectionId = this.sections.findIndex(sections => sections.id === idSection);
    if (event.target.value !== this.sections[sectionId].value.owerview) {
      this.sections[sectionId].value.owerview = event.target.value;
      this.updateSection(this.sections[sectionId].value, sectionId);
    }
  }

  updateSection(section: Section, sectionId) {
    this.sectionService.updateSection(section).subscribe(data => {
      this.sections[sectionId].value = data;
    });
  }

  indicatorUpload () {
    this.dataService.percentDone.subscribe(data => {
        this.percentDone = data;
    });
  }

}
