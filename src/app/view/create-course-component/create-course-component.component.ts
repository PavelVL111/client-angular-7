import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Course} from '../../model/course-model';
import {Section} from '../../model/section-model';
import {VideoService} from '../../service/video.service';
import {SectionService} from '../../service/section.service';
import {CoursesService} from '../../service/courses.service';
import {CreateCourseService} from '../../service/create.course.service';
import {UploadService} from '../../service/upload.service';
import {DataService} from '../../core/data.service';
import {Video} from '../../model/video-model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-course-component',
  templateUrl: './create-course-component.component.html',
  styleUrls: ['./create-course-component.component.css']
})
export class CreateCourseComponent implements OnInit {

  course: Course;
  formGroup: FormGroup;
  formGroup2: FormGroup;

  constructor(private route: ActivatedRoute,
              private coursesService: CoursesService,
              private createCourseService: CreateCourseService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {

    this.formGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });


    this.formGroup2 = this._formBuilder.group({
      firstCtrl2: ['', Validators.required]
    });

    if (this.route.snapshot.params['id'] === 'add') {
      this.createCourse();
    } else {
      this.getCourse();
    }
  }

  createCourse() {
    this.createCourseService.createCourse(this.course).subscribe(data => {
      this.course = data;
    });
  }

  getCourse() {
    this.coursesService.getCourse(this.route.snapshot.params['id']).subscribe(data => {
      this.course = data;
    });
  }

}
