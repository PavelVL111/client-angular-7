import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestsButtonLineComponent } from './tests-button-line.component';

describe('TestsButtonLineComponent', () => {
  let component: TestsButtonLineComponent;
  let fixture: ComponentFixture<TestsButtonLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestsButtonLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestsButtonLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
