import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {CreateTestFormComponent} from '../create-test-form/create-test-form.component';
import {TestService} from '../../../../service/test.service';
import {Test} from '../../../../model/test.model';
import {filter} from 'rxjs/operators';

// type TestArray = Array<{id: number, value: Test}>;

@Component({
  selector: 'app-tests-button-line',
  templateUrl: './tests-button-line.component.html',
  styleUrls: ['./tests-button-line.component.css']
})
export class TestsButtonLineComponent implements OnInit {

  @Input()
  sectionId: number;
  tests: Test[];
  constructor(private testService: TestService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.testService.getTests(this.sectionId).subscribe(data => {
      this.tests = data;
    });
  }

  onClick(test: Test) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = test;

    const dialogRef = this.dialog.open(CreateTestFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      const testId = this.tests.findIndex(testeach => testeach.id === result.id);
      this.tests[testId] = result;
      console.log('response: ' + result.id);
    });
  }

  addTest() {
    this.testService.createTest(this.sectionId).subscribe(data => {
      this.tests.push(data);
    });
  }

  // deleteTest(testId: number) {
  //   this.testService.deleteTest(testId).subscribe(data => {
  //     this.tests.pop(this.findById(testId));
  //   });
  // }

  deleteTest(value: number) {
    const testId = this.tests.findIndex(test => test.id === value);
    this.testService.deleteTest(this.tests[testId].id).subscribe(data => {
      if (data) {
        this.tests.splice(this.tests.findIndex(test => test.id === value), 1);
      }
    });
  }

}
