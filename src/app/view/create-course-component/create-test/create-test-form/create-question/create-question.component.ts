import {Component, Input, OnInit, Output} from '@angular/core';
import {Question} from '../../../../../model/question.model';
import {QuestionService} from '../../../../../service/question.service';
import {Answer} from '../../../../../model/answer.model';
import {Test} from '../../../../../model/test.model';
import {AuthService} from '../../../../../core/auth.service';
import {AnswerService} from '../../../../../service/answer.service';
import {QuestionTypeService} from '../../../../../service/questiontype.service';
import {QuestionType} from '../../../../../model/questiontype.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DataService} from '../../../../../core/data.service';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  @Input()
  question: Question;
  answers: Answer[];
  successUpdate: boolean;
  questionTypes: QuestionType[];
  idRightAnswer: string;
  answerId: number;

  constructor(private questionService: QuestionService,
              private answerService: AnswerService,
              private questionTypeService: QuestionTypeService) {

  }

  ngOnInit() {
    this.getAnswers();
    this.getQuestionTypes();
  }

  getAnswers() {
    this.answerService.getAnswers(this.question.id).subscribe(data => {
      this.answers = data;
    });
  }

  onBlurQuestion(event) {
    this.question.text = event.target.value;
    this.updateQuestion(this.question);
  }

  onChangeQuestionType(event) {
    this.question.questionType = event.value;
    this.updateQuestion(this.question);
    this.setFalseAllRightAnswer();
  }

  onChangeRightRadioButton() {
    this.answerId = this.answers.findIndex(
      answers => answers.id === +this.idRightAnswer);
    this.setFalseAllRightAnswer();
    this.answers[this.answerId].right = true;
    this.updateAnswer(this.answers[this.answerId]);
  }

  onChangeRightCheckbox() {
    this.answers.forEach(answer => this.updateAnswer(answer));
  }

  setFalseAllRightAnswer() {
    this.answers.forEach(answer => {
      if (answer.right) {
        answer.right = false;
        this.updateAnswer(answer);
      }
    });
  }

  updateQuestion(question: Question) {
    this.questionService.updateQuestion(question).subscribe(data => {
      this.successUpdate = data;
    });
  }

  updateAnswer(answer: Answer) {
    this.answerService.updateAnswer(answer).subscribe(data => {
      this.successUpdate = data;
    });
  }

  addAnswer() {
    this.answerService.createAnswer(this.question.id).subscribe(data => {
      this.answers.push(data);
    });
  }

  deleteAnswer(answerId: number) {
    this.answerService.deleteAnswer(answerId).subscribe(data => {
      if (data) {
        this.answers.splice(this.answers.findIndex(
          questions => questions.id === answerId),
          1);
      }
    });
  }

  getQuestionTypes() {
    this.questionTypeService.getQuestionTypes().subscribe(data => {
      this.questionTypes = data;
    });
  }

}
