import {Component, Input, OnInit} from '@angular/core';
import {Answer} from '../../../../../../model/answer.model';
import {AuthService} from '../../../../../../core/auth.service';
import {AnswerService} from '../../../../../../service/answer.service';
import {Question} from '../../../../../../model/question.model';

@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.css']
})
export class CreateAnswerComponent implements OnInit {

  @Input()
  answer: Answer;

  @Input()
  questionType: number;

  updateSuccess: boolean;

  constructor(private answerService: AnswerService) { }

  ngOnInit() {

  }

  onBlurText(event) {
    this.answer.text = event.target.value;
    this.updateAnswer(this.answer);
  }

  updateAnswer(answer: Answer) {
    this.answerService.updateAnswer(answer).subscribe(data => {
      this.updateSuccess = data;
    });
  }

}
