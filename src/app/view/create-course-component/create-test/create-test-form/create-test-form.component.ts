import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import {TestService} from '../../../../service/test.service';
import {QuestionService} from '../../../../service/question.service';
import {Test} from '../../../../model/test.model';
import {Question} from '../../../../model/question.model';

@Component({
  selector: 'app-create-test-form',
  templateUrl: './create-test-form.component.html',
  styleUrls: ['./create-test-form.component.css']
})
export class CreateTestFormComponent implements OnInit {
  testId: number;
  test: Test;
  questions: Question[];
  updateSuccess: boolean;
  constructor(@Inject(MAT_DIALOG_DATA) public testOutside: Test,
              private testService: TestService,
              private questionService: QuestionService,
              ) {
    this.testId = testOutside.id;
  }

  ngOnInit(): void {
    this.testService.getTest(this.testId).subscribe(data => {
        this.test = data;
      this.initQuestion();
    });
  }

  initQuestion() {
    this.questionService.getQuestions(this.test.id).subscribe(data => {
      this.questions = data;
    });
  }

  onBlurTestHeader(event) {
    this.test.header = event.target.value;
    this.updateTest(this.test);
  }

  onBlurTestOverview(event) {
    this.test.owerview = event.target.value;
    this.updateTest(this.test);
  }

  updateTest(test: Test) {
    this.testService.updateTest(test).subscribe(data => {
      this.updateSuccess = data;
      this.testOutside.header = test.header;
    });
  }

  deleteQuestion(questionId: number) {
    this.questionService.deleteQuestion(questionId).subscribe(data => {
      if (data) {
        this.questions.splice(this.questions.findIndex(
          questions => questions.id === questionId),
          1);
      }
    });
  }

  addQuestion() {
    this.questionService.createQuestion(this.testId).subscribe(data => {
      this.questions.push(data);
    });
  }

}
