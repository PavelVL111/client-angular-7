import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {CreateTestFormComponent} from './create-test-form/create-test-form.component';
import {DataService} from '../../../core/data.service';
import {Section} from '../../../model/section-model';
import {TestService} from '../../../service/test.service';


@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.css']
})
export class CreateTestComponent implements OnInit {

  firstFormGroup: FormGroup;
  sections: Array<{id: number, value: Section}>;

  constructor(
              private dataService: DataService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.dataService.courseSections.subscribe( data => {
      this.sections = data;
    });
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
  }


}
