import {Component, Input, OnInit} from '@angular/core';
import {CoursesService} from '../../../service/courses.service';
import {Course} from '../../../model/course-model';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-create-certificate',
  templateUrl: './create-certificate.component.html',
  styleUrls: ['./create-certificate.component.css']
})
export class CreateCertificateComponent implements OnInit {

  @Input()
  course: Course;

  constructor(private courseService: CoursesService) { }

  ngOnInit() {

  }

  agreeCourse() {
    this.courseService.agreeCourse(this.course.id).subscribe(response => {
      if (response instanceof HttpResponse) {
        this.course.agree = true;
      }
    }, error => {
      if (error instanceof HttpErrorResponse) {
        this.course.agree = false;
      }
    });
  }

}
