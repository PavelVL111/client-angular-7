import {Component, Input, OnInit, Output} from '@angular/core';
import {CategoryService} from '../../../../service/category.service';
import {Category} from '../../../../model/category.model';
import {DataService} from '../../../../core/data.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

@Input()
courseCategory: number;

  categorys: Category[];

  constructor(private categoryService: CategoryService,
              private dataService: DataService) { }

  ngOnInit() {
    this.initCategorys();
  }

  initCategorys() {
    this.categoryService.getCategories().subscribe(data => {
      this.categorys = data;
    });
  }

  onChangeCategory(event) {
     this.dataService.setCourseCategory(event.value);
  }

}
