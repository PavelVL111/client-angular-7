import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOwerviewComponent } from './create-owerview.component';

describe('CreateOwerviewComponent', () => {
  let component: CreateOwerviewComponent;
  let fixture: ComponentFixture<CreateOwerviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOwerviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOwerviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
