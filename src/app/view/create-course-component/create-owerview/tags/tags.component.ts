import {Component, Input, OnInit} from '@angular/core';
import {COMMA, ENTER, SPACE} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';
import {TagService} from '../../../../service/tag.service';
import {Tag} from '../../../../model/tag.model';

// export interface Fruit {
//   tag: string;
// }


@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {
  @Input()
  idCourse: number;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  tags: Tag[];
  // tags: Fruit[] = [
  //   {tag: 'Lemon'},
  //   {tag: 'Lime'},
  //   {tag: 'Apple'},
  // ];

  constructor(private tagService: TagService) {}

  ngOnInit(): void {
    this.initTag();
  }

  initTag() {
    this.tagService.getTagsByIdCourse(this.idCourse).subscribe(data => {
      this.tags = data;
    });
  }

  // add(event: MatChipInputEvent): void {
  //   const input = event.input;
  //   const value = event.value;
  //   // Add our tag
  //   if ((value || '').trim()) {
  //     this.tags.push({tag: value.trim()});
  //   }
  //   // Reset the input value
  //   if (input) {
  //     input.value = '';
  //   }
  // }

  addTag(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;
    if (value.trim() === '') {
      return;
    }
    this.tagService.saveTag({id: null, tag: value.trim(), courseId: this.idCourse}).subscribe(data => {
        if ((value || '').trim()) {
          this.tags.push(data);
        }
        // Reset the input value
        if (input) {
          input.value = '';
        }
    });
  }

  remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);
    this.tagService.deleteTag(tag.id).subscribe(data => {
      if (data) {
        if (index >= 0) {
          this.tags.splice(index, 1);
        }
      }
    });
  }

}
