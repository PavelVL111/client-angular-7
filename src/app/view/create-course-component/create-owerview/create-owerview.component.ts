import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {Course} from '../../../model/course-model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {CoursesService} from '../../../service/courses.service';
import {CreateCourseService} from '../../../service/create.course.service';
import {UploadService} from '../../../service/upload.service';
import {DataService} from '../../../core/data.service';

@Component({
  selector: 'app-create-owerview',
  templateUrl: './create-owerview.component.html',
  styleUrls: ['./create-owerview.component.css']
})
export class CreateOwerviewComponent implements OnInit, AfterViewInit {

  @Input()
  course: Course;

  private baseURl = 'http://localhost:8080/';
  isUpdatePhoto: boolean;
  nameImg: string;
  img: HTMLImageElement;
  inputElement: HTMLInputElement;

  formGroup: FormGroup;
  formGroup2: FormGroup;

  constructor(private route: ActivatedRoute,
              private coursesService: CoursesService,
              private createCourseService: CreateCourseService,
              private uploadService: UploadService,
              private dataService: DataService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.changeCourseCategory();
    this.formGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });


    this.formGroup2 = this._formBuilder.group({
      firstCtrl2: ['', Validators.required]
    });
  }

  changeCourseCategory() {
    this.dataService.dataCourseCategory.subscribe(data => {
      this.course.categoryId = data;
      if (data !== -1) {
        this.updateCourse();
      }
    });
  }

  onClick(): void {
    this.inputElement = <HTMLInputElement>document.getElementById('openPhoto');
    this.inputElement.click();
  }

  setUploadPhoto(files: File[]) {
    this.nameImg = files[0].name;
    this.uploadPhoto(files);
  }

  uploadPhoto(files: File[]) {
    this.uploadService.uploadFile(files, this.baseURl + 'create/uploadimage/' + this.course.id);
    this.uploadService.uploadSuccessObservable.subscribe(value => {
      this.isUpdatePhoto = value;
      this.img = <HTMLImageElement> document.getElementsByClassName('course-create-image')[0];
      this.img.src = 'http://localhost:8080/courses/img/' + this.course.id;
    });
  }


  onBlurName(event) {
    if (event.target.value !== this.course.name) {
      this.course.name = event.target.value;
      this.updateCourse();
    }
  }

  onBlurOverview(event) {
    if (event.target.value !== this.course.overview) {
      this.course.overview = event.target.value;
      this.updateCourse();
    }
  }

  onBlurTags(event) {
    if (event.target.value !== this.course.tags) {
      this.course.tags = event.target.value;
      this.updateCourse();
    }
  }

  changeOpen() {
    this.course.open = !this.course.open;
    this.updateCourse();
  }

  updateCourse() {
    this.createCourseService.updateCourse(this.course).subscribe(data => {
      this.course = data;
    });
  }

  ngAfterViewInit(): void {

  }

}
