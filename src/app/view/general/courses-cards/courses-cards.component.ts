import { Component, OnInit } from '@angular/core';
import {CoursesService} from '../../../service/courses.service';
import {Course} from '../../../model/course-model';

@Component({
  selector: 'app-courses-cards',
  templateUrl: './courses-cards.component.html',
  // styleUrls: ['./courses-cards.component.css'],
  styleUrls: ['./courses-style.scss'],
})
export class CoursesCardsComponent implements OnInit {
  // items = ['Apple iPhone 7', 'Huawei Mate 9', 'Samsung Galaxy S7', 'Motorola Moto Z'];
  data: Course[];
  constructor(private coursesService: CoursesService) { }

  ngOnInit() {
    this.coursesService.getCourses().subscribe(
      data => {
        this.data = data;
      }
    );
  }

}
