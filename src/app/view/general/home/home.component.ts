import { Component, OnInit } from '@angular/core';
import {AuthorizationLogerService} from '../../../service/authorization.loger.service';

@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authorizationLogerService: AuthorizationLogerService) { }

  ngOnInit() {
    this.authorizationLogerService.getAuthorizationStatus().subscribe(
      data => {
        console.log(data);
      }
    );
  }

}
