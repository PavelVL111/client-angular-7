import {Component, Input, OnInit} from '@angular/core';
import {DataService} from '../../../core/data.service';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css']
})
export class CourseCardComponent implements OnInit {
  @Input() course;
  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.course.overview = this.course.overview.substr(0, 110) + '...';
  }
  setCurrentIdCourse(id: string): void {
    this.dataService.changeData(id);
  }
}
