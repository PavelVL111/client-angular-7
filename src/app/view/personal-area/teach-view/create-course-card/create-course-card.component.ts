import {Component, Input, OnInit} from '@angular/core';
import {CoursesService} from '../../../../service/courses.service';
import {DataService} from '../../../../core/data.service';

@Component({
  selector: 'app-create-course-card',
  templateUrl: './create-course-card.component.html',
  styleUrls: ['./create-course-card.component.css']
})
export class CreateCourseCardComponent implements OnInit {
  @Input() course;
  constructor(private courseService: CoursesService,
              private  dataService: DataService) { }

  ngOnInit() {
    this.course.overview = this.course.overview.substr(0, 110) + '...';
  }

  deleteCourse(id: number) {
    this.courseService.deleteCourse(id).subscribe(data => {
      if (data) {
        this.dataService.setLastDeleteCourse(id);
      }
    });
  }

}
