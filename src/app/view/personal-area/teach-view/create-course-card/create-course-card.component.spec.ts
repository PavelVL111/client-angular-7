import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCourseCardComponent } from './create-course-card.component';

describe('CreateCourseCardComponent', () => {
  let component: CreateCourseCardComponent;
  let fixture: ComponentFixture<CreateCourseCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCourseCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCourseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
