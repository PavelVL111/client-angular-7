import { Component, OnInit } from '@angular/core';
import {Course} from '../../../model/course-model';
import {CourseBySubscribeUserService} from '../course.by.subscribe.user';
import {CreateCourseService} from '../../../service/create.course.service';
import {DataService} from '../../../core/data.service';
import {SectionService} from '../../../service/section.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-teach-view',
  templateUrl: './teach-view.component.html',
  styleUrls: ['./teach-view.component.css']
})
export class TeachViewComponent implements OnInit {

  courses: Course[];
  course: Course;
  isCreated: boolean;
  constructor(private createCourseService: CreateCourseService,
              private dataService: DataService,
              private sectionService: SectionService,
              private router: Router, ) { }

  ngOnInit() {
    this.getCourseByCreation();
    this.dataService.lastDeleteCourse.subscribe(data => {
      this.getCourseByCreation();
    });
  }

  getCourseByCreation() {
    this.createCourseService.getCoursesByCreateing().subscribe(
      data => {
        this.courses = data;
        if (data.length > 0) {
          this.isCreated = false;
        } else {
          this.isCreated = true;
        }
      }
    );
  }

  createCourse() {
    this.createCourseService.createCourse(this.course).subscribe(data => {
      this.course = data;
      this.router.navigate(['create/' + data.id]);
      // this.createSection();
    });
  }

  // createSection() {
  //   this.sectionService.createSection(this.course.id).subscribe(data => {
  //     if (data !== undefined) {
  //         this.router.navigate(['create/' + data.courseId]);
  //     }
  //   });
  // }

}
