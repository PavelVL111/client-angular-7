import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Course} from '../../model/course-model';
import {AuthCookie} from '../../core/auth.cookie';
import {TokenStorage} from '../../core/token.storage';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CourseBySubscribeUserService {

  constructor(private http: HttpClient, private token: TokenStorage) {}

  private userUrl = 'http://localhost:8080/';

  public getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>(this.userUrl + 'home/coursesbysubscribers');
  }

  public unsubscribe(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.userUrl + 'home/deletsubscribecourse/' + id);
  }

  public cancelUnsubscribe(id: number): Observable<boolean> {
    return this.http.get<boolean>(this.userUrl + 'home/cancelunsubscribecourse/' + id);
  }
}
