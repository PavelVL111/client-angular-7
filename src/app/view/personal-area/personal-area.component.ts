import { Component, OnInit } from '@angular/core';
import {Course} from '../../model/course-model';
import {CoursesService} from '../../service/courses.service';
import {CourseBySubscribeUserService} from './course.by.subscribe.user';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-personal-area',
  templateUrl: './personal-area.component.html',
  styleUrls: ['./personal-area.component.css']
})
export class PersonalAreaComponent implements OnInit {

  data: Course[];
  constructor(private courseBySubscribeUserService: CourseBySubscribeUserService) { }

  ngOnInit() {
    this.courseBySubscribeUserService.getCourses().subscribe(
      data => {
        this.data = data;
      }
    );
  }

}
