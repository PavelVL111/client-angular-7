import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourceLineCardComponent } from './cource-line-card.component';

describe('CourceLineCardComponent', () => {
  let component: CourceLineCardComponent;
  let fixture: ComponentFixture<CourceLineCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourceLineCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourceLineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
