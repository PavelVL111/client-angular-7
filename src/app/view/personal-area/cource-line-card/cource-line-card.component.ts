import {Component, Input, OnInit, Output} from '@angular/core';
import {CourseBySubscribeUserService} from '../course.by.subscribe.user';
import {CertificateService} from '../../../service/certificate.service';

@Component({
  selector: 'app-cource-line-card',
  templateUrl: './cource-line-card.component.html',
  styleUrls: ['./cource-line-card.component.css']
})
export class CourceLineCardComponent implements OnInit {
  @Input() course;
  constructor(private courseBySubscribeUserService: CourseBySubscribeUserService,
              private certificateService: CertificateService) { }

  numberOfDeleteCourse: number;
  styleName = 'course-card';
  isCertificate: boolean;

  ngOnInit() {
    this.course.overview = this.course.overview.substr(0, 110) + '...';
    this.numberOfDeleteCourse = -1;
    this.isCertificated();
  }

  unsubscribe(id: number): void {
    this.courseBySubscribeUserService.unsubscribe(id).subscribe(
      data => {
        if (data === true) {
          this.numberOfDeleteCourse = id;
          this.styleName = 'course-card-hidden';
        } else {
          this.numberOfDeleteCourse = -1;
        }
      }
    );
  }

  cancelUnsubscribe(id: number) {
    this.courseBySubscribeUserService.cancelUnsubscribe(id).subscribe(
      data => {
        if (data === true) {
          this.numberOfDeleteCourse = -1;
          this.styleName = 'course-card';
        } else {
          this.numberOfDeleteCourse = id;
        }
      }
    );
  }

  isCertificated() {
    this.certificateService.isCertificated(this.course.id).subscribe(isCertificate => {
      this.isCertificate = isCertificate;
    });
  }

}
