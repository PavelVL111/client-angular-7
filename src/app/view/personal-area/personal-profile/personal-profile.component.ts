import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDatepickerInputEvent} from '@angular/material';
import {UserService} from '../../../service/app.service';
import {User} from '../../../model/user.model';
import {UploadService} from '../../../service/upload.service';
import {DataService} from '../../../core/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-personal-profile',
  templateUrl: './personal-profile.component.html',
  styleUrls: ['./personal-profile.component.css']
})
export class PersonalProfileComponent implements OnInit {
  private baseURl = 'http://localhost:8080/';
  img: HTMLImageElement;
  mydate = new Date();
  // mydate: Date;
  // date = new FormControl(this.mydate);
  user: User;
  isUdate: boolean;
  isUpdatePhoto: boolean;
  nameImg: string;

  events: string[] = [];
  inputElement: HTMLInputElement;
  constructor(private userService: UserService,
              private uploadService: UploadService,
              private dataService: DataService,
              private router: Router,
  ) {}

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.userService.getUser().subscribe(
      data => {
        this.user = data;
      }
    );
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.mydate.setDate(event.value.getDate());
    this.mydate.setMonth(event.value.getMonth());
    this.mydate.setFullYear(event.value.getFullYear());
    this.user.birthday = this.mydate;
  }

  changeFullname(event: any) {
    this.user.fullname = event.target.value;
  }

  changeNikname(event: any) {
    this.user.nikname = event.target.value;
  }

  public updateUser() {
    this.userService.updateUser(this.user).subscribe(data => {
      this.isUdate = data;
    });
    this.uploadPhoto();
    this.router.navigateByUrl('http://localhost:4200/personalarea');

  }

  onClick(): void {
    this.inputElement = <HTMLInputElement>document.getElementById('openPhoto');
    this.inputElement.click();
  }

  setUploadPhoto(files: File[]) {
    this.dataService.files = files;
    this.nameImg = files[0].name;
  }

  uploadPhoto() {
    this.uploadService.uploadFile(this.dataService.files, this.baseURl + 'uploadavatar');
    this.uploadService.uploadSuccessObservable.subscribe(value => {
      this.isUpdatePhoto = value;
      this.img = <HTMLImageElement> document.getElementsByClassName('mat-card-image')[0];
      this.img.src = 'http://localhost:8080/avatar/' + this.user.avatar;
    });
  }


}
