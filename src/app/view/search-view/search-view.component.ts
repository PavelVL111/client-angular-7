import { Component, OnInit } from '@angular/core';
import {DataService} from '../../core/data.service';
import {Course} from '../../model/course-model';
import {CoursesService} from '../../service/courses.service';

@Component({
  selector: 'app-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.css']
})
export class SearchViewComponent implements OnInit {
  courses: Course[];
  couseName: string;
  constructor(private coursesService: CoursesService,
              private dataService: DataService) { }

  ngOnInit() {
    this.getCourseName();
  }

  getCourseName() {
    this.dataService.dataSearch.subscribe(data => {
      this.couseName = data;
      this.getCourses();
    });
  }

  getCourses() {
    this.coursesService.getCoursesByName(this.couseName).subscribe(
      data => {
        this.courses = data;
      }
    );
  }

}
