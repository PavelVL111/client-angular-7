import {Component, Directive, Input, OnInit} from '@angular/core';
import {VideoService} from '../../../../service/video.service';
import {Video} from '../../../../model/video-model';
import {DataService} from '../../../../core/data.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() id;
  video: Video[];
  @Input()
  posterHidden: {hidden: boolean};
  constructor(private videoService: VideoService,
              private dataService: DataService) { }

  ngOnInit() {
    this.videoService.getVideosBySetion(this.id).subscribe(data => {
      this.video = data;
      this.dataService.setFirstVideoRef(data[0].ref);
    });
  }

  onClick(refVideo: string) {
    this.dataService.setRefVideo(refVideo);
    this.posterHidden.hidden = false;
  }

}
