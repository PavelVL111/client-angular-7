import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../../../../core/data.service';

declare var videojs: any;

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent  implements OnInit, AfterViewInit, OnDestroy {
  private videoJSplayer: any;
  src: string;
  videoElement: HTMLVideoElement;
  source: any;
  firstTime = true;
  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.source = document.getElementById('idsrc');
    // this.videoElement = <HTMLVideoElement>document.getElementsByTagName('video')[0];
    // this.videoElement.src = 'http://localhost:8080/video/Pre_Int_02_90815.mp4';
  }

  ngAfterViewInit(): void {
    this.initVideoJs();
  }

  initVideoJs() {
    this.videoJSplayer = videojs('video_player');
    this.dataService.currentRefVideo.subscribe(data => {
      this.src = 'http://localhost:8080/video/' + data;
      this.videoJSplayer.src(this.src);
      if (this.firstTime) {
        this.firstTime = false;
      } else {
        this.videoJSplayer.play();
      }
    });
  }

  ngOnDestroy() {
    this.videoJSplayer.dispose();
  }


}
