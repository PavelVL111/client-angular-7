import { Component, OnInit } from '@angular/core';
import {Course} from '../../../model/course-model';
import {CoursesService} from '../../../service/courses.service';
import {ActivatedRoute} from '@angular/router';
import {Section} from '../../../model/section-model';
import {SectionService} from '../../../service/section.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../core/data.service';
import {SubscribeCoursesService} from '../../../service/subscribe.courses.service';
import {SupFileService} from '../../../service/supfile.service';
import {SupFileModel} from '../../../model/supfile-model';
import {TestService} from '../../../service/test.service';
import {Test} from '../../../model/test.model';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {CreateTestFormComponent} from '../../create-course-component/create-test/create-test-form/create-test-form.component';
import {PdfViewComponent} from './pdf-view/pdf-view.component';
import {CertificateService} from '../../../service/certificate.service';

type StarValue = Array<{ id: number, name: string }>;

@Component({
  selector: 'app-course-content-card',
  templateUrl: './course-content-card.component.html',
  styleUrls: ['./course-content-card.component.css']
})
export class CourseContentCardComponent implements OnInit {

  firstFormGroup: FormGroup;

  course: Course;
  section: Section[];
  supFiles: SupFileModel[];
  tests: Test[];
  owerviewSection: string;
  headerSection: string;
  isCertificate: boolean;
  posterHidden = {hidden: true};
  numberStar: StarValue = [
    {id: 1, name: 'star_border'},
    {id: 2, name: 'star_border'},
    {id: 3, name: 'star_border'},
    {id: 4, name: 'star_border'},
    {id: 5, name: 'star_border'},
  ];

  constructor(
    private coursesService: CoursesService,
    private sectionService: SectionService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private dataService: DataService,
    private subscribeCoursesService: SubscribeCoursesService,
    private supFileService: SupFileService,
    private testService: TestService,
    private certificateService: CertificateService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // this.dataService.setRefVideo('1');
    this.initCourse();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
  }

  initCourse() {
    this.coursesService.getCourse(this.route.snapshot.params['id']).subscribe(
      data => {
        this.course = data;
        this.initSection();
        this.getCurrentUserEvaluation();
      }
    );
  }

  initSection() {
    this.sectionService.getSectionsByCourse(this.course.id).subscribe(
      data => {
        this.section = data;
        this.setOverviewSection(this.section[0].id);
      }
    );
  }

  initVideos() {

  }

  estimate(evaluation: number) {
    this.subscribeCoursesService.evaluateCourse(this.course.id, evaluation).subscribe(data => {
      if (data === true) {
        this.paintOverStar(evaluation);
      }
    });
  }

  initSupFiles(sectionId: number) {
    this.supFileService.getSupFilesBySetionId(sectionId).subscribe(supFiles => {
      this.supFiles = supFiles;
    });
  }

  initTests(sectionId: number) {
    this.testService.getTests(sectionId).subscribe(tests => {
      this.tests = tests;
      this.isCertificated();
    });
  }

  isCertificated() {
    this.certificateService.isCertificated(this.course.id).subscribe(isCertificate => {
      this.isCertificate = isCertificate;
    });
  }

  getCurrentUserEvaluation() {
    this.subscribeCoursesService.getCurrentUserEvaluation(this.course.id).subscribe(data => {
      this.paintOverStar(data);
    });
  }

  paintOverStar(evaluation: number) {
    for (let i = 0; i <= this.numberStar.length - 1; i++) {
      this.numberStar[i].name = 'star_border';
    }
    for (let i = 0; i <= evaluation - 1; i++) {
      this.numberStar[i].name = 'star';
    }
  }

  setOverviewSection(sectionId: number) {
    const section = this.section[this.section.findIndex(sectionFromAarray => sectionFromAarray.id === sectionId)];
      this.owerviewSection = section.owerview;
      this.headerSection = section.header;
      this.initSupFiles(sectionId);
      this.initTests(sectionId);
  }

  openSupFile(supFileId: number) {
    const dialogConfig = new MatDialogConfig();

    // dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = supFileId;

    this.dialog.open(PdfViewComponent, dialogConfig);
  }

  palyFirst() {
    this.dataService.firstVideoRef.subscribe(firstVideoRef => {
      this.dataService.setRefVideo(firstVideoRef);
      this.posterHidden.hidden = false;
    });
  }

}
