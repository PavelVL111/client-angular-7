import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesByTagComponent } from './courses-by-tag.component';

describe('CoursesByTagComponent', () => {
  let component: CoursesByTagComponent;
  let fixture: ComponentFixture<CoursesByTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesByTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesByTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
