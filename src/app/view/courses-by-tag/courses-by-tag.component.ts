import {Component, OnInit} from '@angular/core';
import {Course} from '../../model/course-model';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../core/data.service';
import {CoursesService} from '../../service/courses.service';

@Component({
  selector: 'app-courses-by-tag',
  templateUrl: './courses-by-tag.component.html',
  styleUrls: ['./courses-by-tag.component.css']
})
export class CoursesByTagComponent implements OnInit {

  public tag: string;

  courses: Course[];

  constructor(private route: ActivatedRoute,
              private couseService: CoursesService
  ) {
  }

  ngOnInit() {
    this.tag = this.route.snapshot.params['id'];
    this.getCoursesByCategory();
  }

  getCoursesByCategory() {
    this.couseService.getCoursesByTag(this.tag).subscribe(data => {
      this.courses = data;
    });
  }

}
