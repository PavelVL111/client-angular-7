import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Blog} from '../../../model/blog.model';
import {BlogService} from '../../../service/blog.service';

@Component({
  selector: 'app-card-post-view',
  templateUrl: './card-post-view.component.html',
  styleUrls: ['./card-post-view.component.css']
})
export class CardPostViewComponent implements OnInit {

  blog: Blog;

  constructor(private route: ActivatedRoute,
              private blogService: BlogService) { }

  ngOnInit() {
    this.blogService.getPost(this.route.snapshot.params['id']).subscribe(data => {
      this.blog = data;
    });
  }

}
