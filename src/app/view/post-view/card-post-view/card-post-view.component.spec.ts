import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPostViewComponent } from './card-post-view.component';

describe('CardPostViewComponent', () => {
  let component: CardPostViewComponent;
  let fixture: ComponentFixture<CardPostViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPostViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPostViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
