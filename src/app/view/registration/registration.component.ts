import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {AuthService} from '../../core/auth.service';
import {TokenStorage} from '../../core/token.storage';
import {AuthCookie} from '../../core/auth.cookie';
import {StatusObject} from '../../core/status.object';
import {RegistrationModel} from '../../model/registration.model';
import {RegistrationService} from '../../service/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  constructor(private router: Router,
              public dialog: MatDialog,
              private authService: AuthService,
              private token: TokenStorage,
              private authCookie: AuthCookie,
              private statusObject: StatusObject,
              private registrationService: RegistrationService,
              private registrationModel: RegistrationModel
  ) {
  }

  username: string;
  password: string;
  passwordagain: string;
  statuspassword = 'Повоторить пароль';
  isSamePassword = false;
  isSuccessfulRegistration: boolean;


  login(): void {
    this.registrationModel.mail = this.username;
    this.registrationModel.password = this.password;
    this.registrationModel.passwordAgain = this.passwordagain;
    this.registrationService.signUp(this.registrationModel).subscribe(
      data => {
        if (<boolean>data) {
          this.router.navigate(['succesfullregistration']);
        }
      }
    );
  }

  equals(): boolean {
    return this.password === this.passwordagain ? true : false;
  }

  ngOnInit(): void {
    this.statuspassword = 'Повоторить пароль';
    this.isSamePassword = true;
  }

}
