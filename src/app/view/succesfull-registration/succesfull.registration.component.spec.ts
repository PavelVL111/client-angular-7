import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Succesfull.RegistrationComponent } from './succesfull.registration.component';

describe('Succesfull.RegistrationComponent', () => {
  let component: Succesfull.RegistrationComponent;
  let fixture: ComponentFixture<Succesfull.RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Succesfull.RegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Succesfull.RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
