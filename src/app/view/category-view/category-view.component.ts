import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../core/data.service';
import {CoursesService} from '../../service/courses.service';
import {Course} from '../../model/course-model';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit {

  public id: number;

  courses: Course[];

  constructor(private route: ActivatedRoute,
              private dataService: DataService,
              private couseService: CoursesService
  ) { }

  ngOnInit() {
    this.dataService.category.subscribe(data => {
      this.id = data;
      if (this.id === 0) {
        this.id = this.route.snapshot.params['id'];
      }
      this.getCoursesByCategory();
    });
  }

  getCoursesByCategory() {
    this.couseService.getCoursesByCategory(this.id).subscribe(data => {
      this.courses = data;
    });
  }

}
